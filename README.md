# Tnk

## Requirements

To run this locally, please install:

* [npm](https://npmjs.com) -- package manager
* [bower](https://bower.io) -- package manager #2
* [yarn](https://yarnpkg.com) -- package manager #3

Once installed, use `yarn install` to get all dependencies sorted out.

## Development server

Run `yarn start` for a dev server. Navigate to `http://localhost:4200/`.
The app will automatically reload if you change any of the source files.

## Local WebRTC Gateway

To run all WebRTC dependencies locally, you need to run janus and coturn.
Either launch them by using the provided scripts in ./scripts or launch
them through a local installation of kubernetes.

### Kubernetes

Follow the direction in the `k8s` folder. Then use the k8s environment
when running your local server, eg

```
$ yarn start --env=k8s
```

### Local installation

If you want to run the services locally, please follow each of its
documentation on how to install;

* [coturn](https://github.com/coturn/coturn) -- TURN server
* [janus](https://janus.conf.meetecho.com/) -- WebRTC gateway

## Browser

When testing WebRTC, you'll probably run into access limitations for the
camera. Access to the camera is exclusive and can't be shared (except
certain browser versions eg Firefox 52).

To test multiple users from one computer, there are other ways around
this limitation.

### Chromium

Chromium have a built-in development fake device for your camera.

```
$ chromium --use-fake-device-for-media-stream
```
