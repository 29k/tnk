// Kubernetes Environment
//
// Copyright (c) 2017 29k International AB

import { AppEnvironment } from '../app/app.environment';
import { generated } from './generated.k8s';

export const environment: AppEnvironment = {
  production: false,
  gateway: [generated.gateway],
  stun: [generated.stun]
};
