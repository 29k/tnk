// Production Environment
//
// Copyright (c) 2017 29k International AB

import { AppEnvironment } from '../app/app.environment';

export const environment: AppEnvironment = {
  production: true,

  // TODO
  gateway: ['ws://127.0.0.1:8188'],
  stun: ['stun:127.0.0.1:3478']
};
