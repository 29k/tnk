#!/bin/sh
#
# coturn-dev.sh launches coturn locally in an
# insecure manner suitable for development.

set -euf

CWD=`dirname $0`
cd $CWD/coturn-dev
exec turnserver -v -c ./conf/turnserver.conf $@
