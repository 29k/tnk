#!/bin/sh
set -e

if [ "$1" = 'turnserver' ]; then
  envsubst < /etc/turnserver.conf > /tmp/turnserver.conf
  mv /tmp/turnserver.conf /etc

  set -- tini -- "$@"
fi

if [ "$1" = 'turnserver' ] && [ "$(id -u)" = '0' ]; then
  set -- gosu turnserver -- "$@"
fi

if [ -z "${COTURN_EXTERNAL_IP}" ]; then
  COTURN_EXTERNAL_IP=`curl -sf4 icanhazip.com`
fi

exec "$@"
