// Room is an abstraction for a video room.
export class Room {
  constructor(public id: string, public janusId: number) {}
}
