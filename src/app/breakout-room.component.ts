import { Component, OnInit, OnDestroy }     from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { LoggerService } from './logger.service';
import { JanusService }  from './janus.service';
import { Room }          from './room';
import { VideoRoom }     from './video-room';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
  selector: 'tnk-breakout-room',
  templateUrl: './breakout-room.component.html',
  styleUrls: ['./breakout-room.component.css']
})
export class BreakoutRoomComponent implements OnInit, OnDestroy {
  // videoRoom is way to make sure we leave the current video room before
  // eg. going back to the big room.
  videoRoom?: Promise<VideoRoom>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private janus: JanusService,
    private log: LoggerService,
  ) {}

  ngOnInit(): void {
    this.route.paramMap
      .filter(params => params.has('id'))
      .map(params => new Room(params.get('id')!, 666))
      .subscribe(room => this.join(room));
  }

  ngOnDestroy(): void {
    this.cleanup();
  }

  cleanup(): Promise<void> {
    if (this.videoRoom) {
      return this.videoRoom
        .then((videoRoom) => videoRoom.leave())
        .then(() => {
          this.videoRoom = undefined;
        });
    } else {
      return Promise.resolve();
    }
  }

  join(room: Room): void {
    this.videoRoom = this.janus.join(room);
  }

  leave(): void {
    this.cleanup()
      .then(() => {
        this.router.navigate(['../..'], {relativeTo: this.route});
      });
  }
}
