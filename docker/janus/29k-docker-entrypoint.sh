#!/usr/bin/env bash
set -e

if [ "$1" = "janus" ]; then
  for f in /29k/etc/janus/*.cfg; do
    envsubst < "$f" > /tmp/subst
    mv /tmp/subst "$f"
  done
fi

exec docker-entrypoint.sh "$@"
