import { enableProdMode }         from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule }    from './app/app.module';
import { setupFromEnv } from './app/app.config';

import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}
setupFromEnv(environment);
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
