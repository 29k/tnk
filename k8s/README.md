# Kubernetes

This folder contains configuration files for kubernetes.

## Requirement

To run this locally, please install:

* [minikube](https://github.com/kubernetes/minikube) -- Minikube

Optionally install these for local tooling:

* [coturn](https://github.com/coturn/coturn) -- TURN server

## Setting up service

Run all services locally by issuing these commands;

```
$ make apply
```

### Test services

Try the STUN-server capabilities from the command line using tools from
coturn.

Since coturn is deployed using `hostNetwork`, the default port `3478`.

```
$ turnutils_stunclient `minikube ip`
0: IPv4. UDP reflexive addr: 192.168.39.1:49376
```
