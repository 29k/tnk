import { Component } from '@angular/core';

@Component({
  template: `<h1>Not Found</h1><div>Go back to <a routerLink="/">start</a>.</div>`,
})

// NotFoundComponent
//
// This is based on the [routing guide] found on angular.io. Weird to not set
// the HTTP status code to 404, but hey -- welcome to nodejs. :)
//
// [routing guide]: https://angular.io/guide/router
export class NotFoundComponent { }
