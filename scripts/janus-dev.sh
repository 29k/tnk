#!/bin/sh
#
# janus-dev.sh launches janus locally in an
# insecure manner suitable for development.

set -euf

CWD=`dirname $0`
cd $CWD/janus-dev
exec janus \
  --config ./conf/janus.cfg \
  --configs-folder=./conf $@
