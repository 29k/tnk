import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG, AppConfig } from './app.config';
import { LoggerService }         from './logger.service';
import { Room }                  from './room';
import { VideoRoom }             from './video-room';

// HACK: janus is imported through the .angular-cli.json file.
import * as adapter from "webrtc-adapter";

/// <reference path="janus.d.ts" />

// JanusService is an abstraction over janus.
//
// Every communication with Janus goes first through this Angular service, on
// to the Janus JavaScript library provided by meetecho.
@Injectable()
export class JanusService {
  janus?: Janus = undefined;
  debug: Janus.DebugMode = ['trace', 'log', 'warn', 'error'];

  // userMedia holds a reference to the user media stream once a user have
  // allowed us to use it. This is useful to prevent us from asking for
  // access to the users camera and audio more than once per session.
  userMedia?: MediaStream = undefined;

  // TODO: should be mapped to the user id
  opaqueId = Janus.randomString(12);

  constructor(
    // HACK(orion): '| AppConfig' is a compiler warning workaround
    // https://github.com/angular/angular-cli/issues/2034
    @Inject(APP_CONFIG) private config: AppConfig | AppConfig,
    private log: LoggerService,
  ) {}

  // connect makes sure janus is connected.
  connect(): Promise<Janus> {
    // TODO: add retry logic with backoff
    let dependencies = Janus.useDefaultDependencies({
      adapter: adapter
    });

    // TODO: this is not idempotent
    return new Promise<Janus>((resolve, reject) => {
      Janus.init({
        debug: this.debug,
        dependencies: dependencies,

        callback: () => {
          // handle previous connect and multiple simultaneous calls
          if (this.janus) {
            this.log.debug('janus: already initialised');
            resolve(this.janus);
            return;
          }

          this.log.debug('janus: initialised', this.config);
          this.janus = new Janus({
            server: this.config.webrtc.gateway,
            iceServers: this.config.webrtc.iceServers,
            success: () => {
              this.log.debug('janus: successfully connected');
              resolve(this.janus);
            },
            // FIXME: We cannot call reject() here since some paths in janus
            //        does retries.
            error: (msg) => this.log.error('janus: error', msg),
          });
        }
      });
    });
  }

  // getUserMedia queries the browser for audio and video devices.
  //
  // We want to fetch the media stream from the browser ourself instead of
  // letting janus.js doing it. Simply because we want to re-use the same
  // stream for both the group video and 1:1 video.
  getUserMedia(): Promise<MediaStream> {
    // TODO: can we check if the stream is still valid?
    if (this.userMedia) {
      return Promise.resolve(this.userMedia);
    }

    // TODO: unmute audio
    let constraints = { audio: false, video: { facingMode: "user" } };
    return navigator.mediaDevices.getUserMedia(constraints)
      .then((userMedia) => {
        this.userMedia = userMedia;
        return userMedia;
      });
  }

  // join joins a separate video room.
  join(room: Room): Promise<VideoRoom> {
    let local: LocalVideoChannel | undefined;
    let remote: RemoteVideoPublishGroup | undefined;

    // TODO: add console.time() for connect and attach et al
    return new Promise<VideoRoom>((resolve, reject) => {
      this.connect()
        .then(() => this.getUserMedia())
        .then((stream) => {
          if (!this.janus) {
            this.log.error('Expected Janus to be defined.');
            return;
          }

          this.log.info('Connected to video service (id: %s). Joining room %o...', this.janus.getSessionId(), room);

          this.janus.attach({
            plugin: 'janus.plugin.videoroom',
            opaqueId: this.opaqueId,

            // successfully created handle
            success: (pluginHandle) => {
              local = new LocalVideoChannel(this.log, room, pluginHandle);
              local.join();

              resolve(new JanusVideoRoom(local));
            },

            // failed to create a handle
            error: (cause) => {
              this.log.error('attach: error', cause);
              reject(cause);
            },

            webrtcState: (associated, reason) => {
              this.log.debug('attach: webrtc-state associated=%s, reason=%o', associated, reason);
            },
            iceState: (state) => this.log.debug('attach: ice-state', state),
            mediaState: (mediaType, on) => this.log.debug('attach: media-state', mediaType, on),
            slowLink: (uplink) => this.log.debug('attach: slow-link', uplink),

            // message pushed through control channel
            onmessage: (message, jsep) => {
              this.log.debug('attach: message', message, jsep);

              switch (message.videoroom) {
              case 'joined':
                console.log('joined');
                if (local) {
                  local.offerMedia(stream);
                }

                if (this.janus) {
                  remote = new RemoteVideoPublishGroup(this.janus, this.log, room, message.private_id, this.opaqueId);
                }

                // subscribe to all remote publishers who were already there
                if (message.publishers instanceof Array) {
                  if (remote) {
                    remote.subscribe(message.publishers);
                  }
                }
                break;
              case 'destroyed':
                break;
              case 'event':
                // subscribe to remote publishers who just joined
                if (message.publishers instanceof Array) {
                  if (remote) {
                    remote.subscribe(message.publishers);
                  }
                } else if (message.leaving) {
                  // TODO: remove remote feed
                  console.log('leaving');
                  if (remote) {
                    remote.unsubscribe(message.leaving);
                  }
                } else if (message.unpublished) {
                  // TODO: remove remote feed
                  console.log('unpublished');
                  // response 'ok' is unpublished on our side
                  if (message.unpublished == 'ok') {
                    if (local) {
                      local.hangup();
                    }
                  } else {
                    if (remote) {
                      remote.unsubscribe(message.unpublished);
                    }
                  }
                } else if (message.configured) {
                  console.log('configuration confirmed');
                } else if (message.error) {
                  this.log.error('Event error', message.error);
                }

                if (jsep !== undefined && jsep !== null) {
                  if (local) {
                    local.sessionData(jsep);
                  }

                  if (!message.audio_codec) {
                    this.log.warn('Audio stream has been rejected?');
                  }
                  if (!message.video_codec) {
                    this.log.warn('Video stream has been rejected?');
                  }
                }

                break;
              }
            },

            // attach our local stream
            onlocalstream: (stream) => {
              this.log.debug('room: attaching local stream..');
              if (local) {
                local.create(stream);
              }
            },

            // publishing side does not have remote streams
            onremotestream: (stream) => {
              console.assert(false, 'remote stream was unexpected');
            },

            ondataopen: () => this.log.debug('attach: on-data-open'),
            ondata:     () => this.log.debug('attach: on-data'),
            oncleanup:  () => this.log.debug('attach: on-clean-up'),
            detached:   () => this.log.debug('attach: detached'),
          });
        });
    });
  }

}

class JanusVideoRoom implements VideoRoom {
  constructor(
    public local: LocalVideoChannel,
  ) {}

  leave(): Promise<void> {
    this.local.hangup();
    // TODO: listen to hangup state
    return Promise.resolve();
  }
}

class LocalVideoChannel {
  constructor(
    // TODO: inject?
    public log: LoggerService,
    public room: Room,
    public handle: PluginHandle,
  ) {}

  hangup(): void {
    this.log.debug('local-video: calling hangup');
    this.leave();
    this.handle.hangup();
  }

  // TODO: rename to bind()? or just publish?
  join(): void {
    // TODO: room should be respected -- 1234 is by default created as a
    // demo room with a maximum number of 6 participants
    let username = "Jane Doe " + window.navigator.userAgent; // TODO(orion)
    let register = { "request": "join", "room": this.room.janusId, "ptype": "publisher", "display": username };
    this.handle.send({"message": register});
  }

  leave(): void {
    let unregister = { "request": "leave", "room": this.room.janusId};
    this.handle.send({"message": unregister});
  }

  // offerMedia offers our local audio and video from the stream but don't
  // receive anything.
  offerMedia(stream: MediaStream): void {
    this.handle.createOffer({
      stream: stream,
      success: (jsep) => {
        this.log.debug('create-offer: success', jsep);
        let publish = { "request": "configure", "audio": true, "video": true };
        this.handle.send({"message": publish, "jsep": jsep});
      },
      error: () => {
        // TODO: show error to user
        this.log.debug('create-offer: error');
      }
    });
  }

  create(stream: MediaStream): void {
    // TODO: this is not how angular works. we should add it to some state which is exposed
    // to the template. Angular will then detect when things changes.
    let el = document.getElementById('video-local');
    if (el) {
      Janus.attachMediaStream(el, stream);
    }
  }

  destroy(): void {
    // TODO
  }

  // jsep the JavaScript Session Establishment Protocol is handled.
  sessionData(jsep: any): void {
    this.handle.handleRemoteJsep({jsep: jsep});
  }
}

class RemoteVideoPublishGroup {
  remotes = {};

  constructor(
    // TODO: inject?
    public janus: Janus,
    public log: LoggerService,
    public room: Room,
    public privateId: string,
    public opaqueId: string,
  ) {}

  subscribe(publishers: any[]): void {
    for (let p of publishers) {
      this.remotes[p.id] = this.attachRemote(this.privateId, p.id, p.display, p.audio_codec, p.video_codec);
    }
  }

  unsubscribe(id: string): void {
    let promise = this.remotes[id];

    if (promise !== undefined && promise !== null) {
      promise.then((remote) => {
        remote.destroy();
      });
    }
  }

  attachRemote(privateId: string, id: string, display: string, audio: string, video: string): Promise<RemoteVideoPublisher> {
    let remote = new Promise<RemoteVideoPublisher>((resolve, reject) => {
      this.janus.attach({
        plugin: 'janus.plugin.videoroom',
        opaqueId: this.opaqueId,

        success: (pluginHandle) => {
          this.log.debug('Subscribed as listener', pluginHandle.getId());
          let publisher = new RemoteVideoPublisher(this.log, this.room, pluginHandle, id, this.privateId, this.opaqueId);
          publisher.listen();
          resolve(publisher);
        },

        error: (cause) => {
          this.log.error('Failed to attach remote:', cause);
          reject(cause);
        },

        webrtcState: (associated, reason) => {
          this.log.debug('remote attach: webrtc-state associated=%s, reason=%o', associated, reason);
        },
        iceState: (state) => this.log.debug('remote attach: ice-state', state),
        mediaState: (mediaType, on) => this.log.debug('remote attach: media-state', mediaType, on),
        slowLink: (uplink) => this.log.debug('remote attach: slow-link', uplink),

        onmessage: (message, jsep) => {
          this.log.debug('remote attach: on message', message, jsep);
          if (message.error !== undefined && message.error !== null) {
            this.log.error('remote attach: on message error', message.error);
          } else {
            switch (message.videoroom) {
            case 'attached':
              // TODO: start spinner for remote video, or should it be done in onremotestream?
              this.log.debug('Attached to remote feed', message.id, message.display);
              break;
            case 'event':
              this.log.debug('Event received for remote stream', message);
              break;
            default:
              this.log.debug('Unhandled remote message:', message, jsep);
              break;
            }
          }

          if (jsep !== undefined && jsep !== null) {
            remote.then((publisher) => publisher.answer(jsep));
          }
        },

        // listening side does not have local streams
        onlocalstream: (stream) => {
          console.assert(false, 'local stream was unexpected in remote feed');
        },

        // attach someone's remote stream
        onremotestream: (stream) => {
          this.log.debug('Remote WebRTC stream available', stream);
          remote.then((publisher) => publisher.create(stream));
        },

        ondataopen: () => this.log.debug('remote attach: on-data-open'),
        ondata:     () => this.log.debug('remote attach: on-data'),
        oncleanup:  () => {
          // TODO: remove remote video
          this.log.debug('remote attach: on-cleanup');
          remote.then((publisher) => publisher.destroy());
        },

        detached: () => this.log.debug('remote attach: detached'),
      })
    });

    return remote;
  }
}

class RemoteVideoPublisher {
  private el?: HTMLVideoElement;

  constructor(
    // TODO: inject?
    public log: LoggerService,
    public room: Room,
    public handle: PluginHandle,
    public id: string,
    public privateId: string,
    public opaqueId: string,
  ) {}

  listen(): void {
    let listen = {
      request:    "join",
      room:       this.room.janusId,
      ptype:      "listener",
      feed:       this.id,
      private_id: this.privateId
    };
    this.handle.send({"message": listen});
  }

  create(stream: MediaStream): void {
    // TODO: this is not how angular works. we should add it to some state which is exposed
    // to the template. Angular will then detect when things changes.
    this.el = document.createElement('video');
    this.el.className = 'video-remote';
    // TODO: make fluid and use css
    this.el.width = 160;
    this.el.height = 120;
    this.el.setAttribute('autoplay', '');
    this.el.setAttribute('playsinline', '');
    let cont = document.getElementById('video-remote-container');
    if (cont) {
      cont.appendChild(this.el);
    }

    Janus.attachMediaStream(this.el, stream);
  }

  destroy(): void {
    let cont = document.getElementById('video-remote-container');
    if (cont && this.el) {
      cont.removeChild(this.el);
      this.el = undefined;
    }
  }

  answer(jsep: any): void {
    this.log.debug('Creating answer');

    // Create answer which only receives audio and video.
    this.handle.createAnswer({
      jsep: jsep,
      media: { audioSend: false, videoSend: false },
      success: (recvJsep) => {
        this.log.debug('Recieved SDP for answer', jsep);
        let body = {"request": "start", "room": this.room.janusId};
        this.handle.send({"message": body, "jsep": recvJsep});
      },
      error: (error) => {
        this.log.error('Failed to create answer', error);
      }
    });
  }
}
