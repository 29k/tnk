import { Injectable } from '@angular/core';

@Injectable()
export class LoggerService {
  trace(message?: any, ...optionalParams: any[]): void {
    console.trace(message, ...optionalParams);
  }
  debug(message?: any, ...optionalParams: any[]): void {
    console.log(message, ...optionalParams);
  }
  info(message?: any, ...optionalParams: any[]): void {
    console.info(message, ...optionalParams);
  }
  warn(message?: any, ...optionalParams: any[]): void {
    console.warn(message, ...optionalParams);
  }
  error(message?: any, ...optionalParams: any[]): void {
    console.error(message, ...optionalParams);
  }
}
