import { Component } from '@angular/core';
import { Router }    from '@angular/router';

@Component({
  selector: 'tnk-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})

export class RootComponent {
  room: string | null;

  constructor(
    private router: Router,
  ) { }

  join(): void {
    if (this.room) {
      this.router.navigate(['/room', this.room]);
    }
  }
}
