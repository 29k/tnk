// Copyright (c) 2017 29k International AB

// AppEnvironment represents settings based on the environment when
// application is started.
export interface AppEnvironment {
  // production mode
  production: boolean;
  // WebRTC gateway
  gateway: string[];
  // STUN servers
  stun: string[];
}
