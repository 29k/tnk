# Network

The use of Kubernetes brings challenges to services not built for beeing
exposed behind it. Here are documented known exceptions and limitations;

| Name               | Port            |
| ------------------ |:---------------:|
| Janus -- RTP, RTCP | 31000-31008 UDP |
| coturn -- relay    | 32160-32168 UDP |
