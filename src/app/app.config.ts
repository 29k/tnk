// Application Configuration
//
// Copyright (c) 2017 29k International AB

/// <reference types="webrtc" />

import { InjectionToken } from '@angular/core';

import { AppEnvironment } from './app.environment';

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// AppConfig holds the configuration for the application.
export interface AppConfig {
  webrtc: WebRTCConfig;
}

// WebRTCConfig is the WebRTC configuration.
export interface WebRTCConfig {
  gateway: string[];
  iceServers: RTCIceServer[];
}

// TODO: expose the config using a function? the problem we have now is
// that if we alter the actual config variable, code that have already
// taken a reference to this object won't get the new configuration.
export var config: AppConfig = {
  webrtc: {
    gateway: [],
    iceServers: []
  }
};

// setupFromEnv sets the configuration up based on the provided
// application environment.
export function setupFromEnv(environment: AppEnvironment) {
  config.webrtc = {
    gateway: environment.gateway,
    iceServers: environment.stun.map(server => {
      return {urls: server}
    })
  }
}
