import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { APP_CONFIG, config } from './app.config';

import { AppRoutingModule } from './app.routing.module';

import { JanusService }     from './janus.service';
import { LoggerService }    from './logger.service';

import { AppComponent }          from './app.component';
import { BreakoutRoomComponent } from './breakout-room.component';
import { NotFoundComponent }     from './not-found.component';
import { RoomComponent }         from './room.component';
import { RootComponent }         from './root.component';

@NgModule({
  declarations: [
    AppComponent,
    BreakoutRoomComponent,
    NotFoundComponent,
    RoomComponent,
    RootComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [
    { provide: APP_CONFIG, useValue: config },
    JanusService,
    LoggerService,
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
