import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RootComponent }         from './root.component';
import { RoomComponent }         from './room.component';
import { BreakoutRoomComponent } from './breakout-room.component';
import { NotFoundComponent }     from './not-found.component';

const routes: Routes = [
  { path: '', component: RootComponent },
  { path: 'room/:id', component: RoomComponent },
  { path: 'room/:id/breakout/x', component: BreakoutRoomComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})

export class AppRoutingModule {}
