/// <reference types="webrtc" />

// Janus WebRTC Gateway declaration.
declare namespace Janus {
  interface JanusDependencies {}
  interface JanusDefaultDependenciesOptions {
    Promise?: any;
    adapter?: any;
  }
  function useDefaultDependencies(deps?: JanusDefaultDependenciesOptions): JanusDependencies;

  type DebugLevel = 'trace' | 'debug' | 'vdebug' | 'log' | 'warn' | 'error';
  type DebugMode = 'all' | boolean | DebugLevel[];

  interface JanusInitOptions {
    debug?: DebugMode;
    dependencies?: JanusDependencies;
    callback?: () => void;
  }
  function init(options?: JanusInitOptions): void;

  function isWebrtcSupported(): boolean;
  function randomString(len: number): string;
  function listDevices(callback: (devices: MediaDeviceInfo[]) => void, constraints?: MediaStreamConstraints): void;
  function attachMediaStream(element: HTMLElement, stream: MediaStream): void;
}

interface JanusOptions {
  server: string | string[];
  iceServers?: RTCIceServer[];
  iceTransportPolicy?: RTCIceTransportPolicy;
  bundlePolicy?: RTCBundlePolicy;

  ipv6?: any;
  withCredentials?: any;
  max_poll_events?: any;
  token?: any;
  apisecret?: any;
  destroyOnUnload?: any;

  success?: () => void;
  error?: (message: string) => void;
  destroyed?: () => void;
}

interface VideoObject {
  deviceId: string;
  width: number;
  height: number;
}

type MediaVideoFormat = 'lowres' | 'lowres-16:9' | 'stdres' | 'stdres-16:9' | 'hires' | 'hires-16:9';
type MediaVideo = MediaVideoFormat | 'screen' | VideoObject | boolean;

interface MediaOptions {
  audio?: boolean;
  audioSend?: boolean;
  audioRecv?: boolean;
  video?: MediaVideo;
  videoSend?: boolean;
  videoRecv?: boolean;
  data?: boolean;
  failIfNoAudio?: boolean;
  failIfNoVideo?: boolean;
  // screenshareFrameRate
}

interface CreateOfferOptions {
  media?: MediaOptions;
  trickle?: boolean;
  stream?: MediaStream;

  success?: (jsep: any) => void;
  error?: (message: string) => void;
}

interface CreateAnswerOptions {
  media?: MediaOptions;
  trickle?: boolean;
  stream?: MediaStream;
  jsep: any;

  success?: (jsep: any) => void;
  error?: (message: string) => void;
}

// TODO
interface PluginHandle {
  getId(): string;
  getPlugin(): string;
  // getVolume
  isAudioMuted(): boolean;
  muteAudio(): void;
  unmuteAudio(): void;
  isVideoMuted(): boolean;
  muteVideo(): void;
  unmuteVideo(): void;
  // getBitrate
  send(request: any): void;
  // data
  // dtmf
  createOffer(options?: CreateOfferOptions): void;
  createAnswer(options?: CreateAnswerOptions): void;
  // TODO: which arguments does handleRemoteJsep take?
  handleRemoteJsep(f: any): void;
  hangup(hangupRequest?: boolean): void;
  // detach
}

interface AttachOptions {
  plugin: string;
  opaqueId: string;
  success: (pluginHandle: PluginHandle) => void;
  error: (message: string) => void;
  consentDialog?: (on: boolean) => void;
  iceState?: (evt: Event) => void;
  mediaState?: (mediaType: string, on: boolean) => void;
  webrtcState?: (associated: boolean, reason: string) => void;
  // TODO: define nacks
  slowLink?: (uplink: boolean, nacks: any) => void;
  // TODO: define jsep
  // TODO: define message
  onmessage?: (message: any, jsep: any) => void;
  onlocalstream?: (stream: MediaStream) => void;
  onremotestream?: (stream: MediaStream) => void;
  // TODO: define data
  ondata?: (data: any) => void;
  ondataopen?: () => void;
  oncleanup?: () => void;
  detached?: () => void;
}

declare class Janus {
  constructor(options?: JanusOptions);

  getSessionId(): string;
  attach(options: AttachOptions): void;
}
