// Development Environment
//
// This file contains the development environment.
//
// During build -- this file, within the build output directory, will be
// overwritten to contain whatever environment is used when running the
// application `ng build --env=prod`.
//
// See `.angular-cli.json` for more information.
//
// Copyright (c) 2017 29k International AB

import { AppEnvironment } from '../app/app.environment';

export const environment: AppEnvironment = {
  production: false,

  gateway: ['ws://127.0.0.1:8188'],
  stun: ['stun:127.0.0.1:3478']
};
